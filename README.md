Instruções para teste 

 

Pré-requisitos: 

-É necessário possuir Maven instalado e configurado no sistema; 

-É necessário ter o JDK (8) instalado e configurado no sistema; 

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Como executar: 

-Abrir o projeto, abrir a classe de testes Pokemon, localizada em: 

TesteSouth\src\test\java\CTs 

-Clickar com botão direito e selecionar: 

Run ‘Pokemon’ 

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Casos de Teste: 

Pokemon - verificarRetornoPokemonPositivo() 

Neste teste, será verificado se a API retornara status 200. Isso ocorre se a lista de Pokemon estiver com todos os registros inscritos com parâmetros válidos 

 

Pokemon - verificarRetornoPokemonNegativo() 

Neste teste, será verificado se a API retornara status 404. Isso ocorre se a lista de Pokemon estiver com todos os registros inscritos com parâmetros inválidos 

 

Pokemon - verificarRetornoTipoPokemonPositivo() 

Neste teste, será verificado se a API retornara status 200. Isso ocorre se a lista de tipos possíveis de Pokemon estiver com todos os registros inscritos com parâmetros válidos 

 

Pokemon - verificarRetornoTipoPokemonNegativo() 

Neste teste, será verificado se a API retornara status 404. Isso ocorre se a lista de tipos possíveis de Pokemon estiver com todos os registros inscritos com parâmetros inválidos 

 

Pokemon - verificarRetornoFrutaPositivo() 

Neste teste, será verificado se a API retornara status 200. Isso ocorre se a lista de frutas do universo Pokemon estiver com todos os registros inscritos com parâmetros válidos 

 

Pokemon - verificarRetornoFrutaNegativo() 

Neste teste, será verificado se a API retornara status 404. Isso ocorre se a lista de frutas do universo Pokemon estiver com todos os registros inscritos com parâmetros inválidos 

 

Pokemon - verificarRetornoGolpePositivo() 

Neste teste, será verificado se a API retornara status 200. Isso ocorre se a lista de golpes do universo Pokemon estiver com todos os registros inscritos com parâmetros válidos 

 

Pokemon - verificarRetornoGolpeNegativo() 

Neste teste, será verificado se a API retornara status 404. Isso ocorre se a lista de golpes do universo Pokemon estiver com todos os registros inscritos com parâmetros inválidos 

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 

Pós testes: 

-É possível acessar o relatório dos testes no diretório pela IDE, na pasta: 

target/site/surefire-report.html 

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 

Observações:  

-Não consegui ter muito tempo para fazer a prova, apesar do feriado de carnaval. Ainda tenho alguns problemas para configurar os diretórios para o maven rodar pelo terminal 