package Api;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.junit.Assert;


public class PokeApi
{
    public Response requisitarPokemon(String pokemon)
    {
        return getRequest("/pokemon", pokemon);
    }

    public Response requisitarPokemon(int pokemon)
    {
        return getRequest("/pokemon", Integer.toString(pokemon));
    }

    public Response requisitarTipo(String tipo)
    {
        return getRequest("/type", tipo);
    }

    public Response requisitarTipo(int tipo)
    {
        return getRequest("/type", Integer.toString(tipo));
    }

    public Response getRequest(String basePath, String parameter)
    {
        RestAssured.baseURI = "https://pokeapi.co/api/v2";
        RestAssured.basePath = basePath;
        RequestSpecification request = RestAssured.given();
        Response response = request.accept(ContentType.JSON).get(parameter);
        return response;
    }

    public Response requisitarGolpe(String golpe)
    {
        return getRequest("/move", golpe);
    }

    public Response requisitarGolpe(int golpe)
    {
        return getRequest("/move", Integer.toString(golpe));
    }

    public Response requisitarFruta(String fruta)
    {
        return getRequest("/berry", fruta);
    }

    public Response requisitarFruta(int fruta)
    {
        return getRequest("/berry", Integer.toString(fruta));
    }
}
