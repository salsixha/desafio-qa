package CTs;


import Api.PokeApi;
import org.junit.*;
import utils.Arquivo;

import java.io.FileNotFoundException;
import java.util.List;

public class Pokemon
{
    PokeApi pokeApi = new PokeApi();
    List<String[]> conteudoArquivo;

    @Test
    public void verificarRetornoPokemonPositivo() throws FileNotFoundException
    {
        conteudoArquivo = Arquivo.LerArquivo("listaPokemonsPositivo.csv");
        for (int i = 0; i < conteudoArquivo.size(); i++)
        {
            System.out.println("Verificando linha " + (i+1) + " ...");
            Assert.assertTrue(pokeApi.requisitarPokemon(conteudoArquivo.get(i)[0]).getStatusCode() == 200);
            System.out.println("Linha " + (i+1) + " verificada com sucesso");
        }
    }

    @Test
    public void verificarRetornoPokemonNegativo() throws FileNotFoundException
    {
        conteudoArquivo = Arquivo.LerArquivo("listaPokemonsNegativo.csv");
        for (int i = 0; i < conteudoArquivo.size(); i++)
        {
            System.out.println("Verificando linha " + (i+1) + " ...");
            Assert.assertTrue(pokeApi.requisitarPokemon(conteudoArquivo.get(i)[0]).getStatusCode() == 404);
            System.out.println("Linha " + (i+1) + " verificada com sucesso");
        }
    }

    @Test
    public void verificarRetornoTipoPokemonPositivo() throws FileNotFoundException
    {
        conteudoArquivo = Arquivo.LerArquivo("listaTipoPokemonsPositivo.csv");
        for (int i = 0; i < conteudoArquivo.size(); i++)
        {
            System.out.println("Verificando linha " + (i+1) + " ...");
            Assert.assertTrue(pokeApi.requisitarTipo(conteudoArquivo.get(i)[0]).getStatusCode() == 200);
            System.out.println("Linha " + (i+1) + " verificada com sucesso");
        }
    }

    @Test
    public void verificarRetornoTipoPokemonNegativo() throws FileNotFoundException
    {
        conteudoArquivo = Arquivo.LerArquivo("listaTipoPokemonsNegativo.csv");
        for (int i = 0; i < conteudoArquivo.size(); i++)
        {
            System.out.println("Verificando linha " + (i+1) + " ...");
            Assert.assertTrue(pokeApi.requisitarTipo(conteudoArquivo.get(i)[0]).getStatusCode() == 404);
            System.out.println("Linha " + (i+1) + " verificada com sucesso");
        }
    }

    @Test
    public void verificarRetornoFrutaPositivo() throws FileNotFoundException
    {
        conteudoArquivo = Arquivo.LerArquivo("listaFrutaPositivo.csv");
        for (int i = 0; i < conteudoArquivo.size(); i++)
        {
            System.out.println("Verificando linha " + (i+1) + " ...");
            Assert.assertTrue(pokeApi.requisitarFruta(conteudoArquivo.get(i)[0]).getStatusCode() == 200);
            System.out.println("Linha " + (i+1) + " verificada com sucesso");
        }
    }

    @Test
    public void verificarRetornoFrutaNegativo() throws FileNotFoundException
    {
        conteudoArquivo = Arquivo.LerArquivo("listaFrutaNegativo.csv");
        for (int i = 0; i < conteudoArquivo.size(); i++)
        {
            System.out.println("Verificando linha " + (i+1) + " ...");
            Assert.assertTrue(pokeApi.requisitarFruta(conteudoArquivo.get(i)[0]).getStatusCode() == 404);
            System.out.println("Linha " + (i+1) + " verificada com sucesso");
        }
    }

    @Test
    public void verificarRetornoGolpePositivo() throws FileNotFoundException
    {
        conteudoArquivo = Arquivo.LerArquivo("listaGolpePokemonsPositivo.csv");
        for (int i = 0; i < conteudoArquivo.size(); i++)
        {
            System.out.println("Verificando linha " + (i+1) + " ...");
            Assert.assertTrue(pokeApi.requisitarGolpe(conteudoArquivo.get(i)[0]).getStatusCode() == 200);
            System.out.println("Linha " + (i+1) + " verificada com sucesso");
        }
    }

    @Test
    public void verificarRetornoGolpeNegativo() throws FileNotFoundException
    {
        conteudoArquivo = Arquivo.LerArquivo("listaGolpePokemonsNegativo.csv");
        for (int i = 0; i < conteudoArquivo.size(); i++)
        {
            System.out.println("Verificando linha " + (i+1) + " ...");
            Assert.assertTrue(pokeApi.requisitarGolpe(conteudoArquivo.get(i)[0]).getStatusCode() == 404);
            System.out.println("Linha " + (i+1) + " verificada com sucesso");
        }
    }
}
